package cl.psepulveda.globallogictest.framework.net;

import cl.psepulveda.globallogictest.framework.net.dto.GetAlbumDetailResponse;
import cl.psepulveda.globallogictest.framework.net.dto.SearchTrackResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ITunesApiServices {

    @GET(HttpConstants.SEARCH)
    Call<SearchTrackResponse> searchTrack(@Query("term") String searchText, @Query("offset") int offset, @Query("mediaType") String mediaType, @Query("limit") int limit);

    @GET(HttpConstants.ALBUM)
    Call<GetAlbumDetailResponse> getAlbumById(@Query("id") Long id, @Query("entity") String entity);

}
