package cl.psepulveda.globallogictest.ui.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.List;

import cl.psepulveda.domain.Track;
import cl.psepulveda.globallogictest.R;
import cl.psepulveda.globallogictest.ui.adapter.TrackListAdapter;
import cl.psepulveda.globallogictest.ui.presenter.SearchPresenter;

public class SearchActivity extends BaseActivity {

    private static final int SPAN_COUNT = 3;
    private static final int MAX_RESULT_COUNT = 500;

    private SearchPresenter presenter;

    private EditText etSearch;
    private ImageView ivSearch;
    private RecyclerView rvTrackList;

    private TrackListAdapter trackListAdapter;

    private String currentSearchText = "";
    private int currentSearchOffset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        presenter = new SearchPresenter(this);
        presenter.create(savedInstanceState);

        etSearch = findViewById(R.id.etSearch);
        ivSearch = findViewById(R.id.ivSearch);
        rvTrackList = findViewById(R.id.rvTrackList);

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentSearchText = etSearch.getText().toString();
                currentSearchOffset = 0;
                presenter.searchButtonClicked(currentSearchText, currentSearchOffset);
            }
        });

        rvTrackList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (trackListAdapter != null && !recyclerView.canScrollVertically(LinearLayoutManager.VERTICAL) && trackListAdapter.getItemCount() < MAX_RESULT_COUNT) {
                    presenter.loadMore(currentSearchText, currentSearchOffset);
                }
            }
        });
    }

    public void renderTrackList(List<Track> trackList) {
        if(trackList.size() == 0) {
            showToast(getString(R.string.result_not_found));
        }

        trackListAdapter = new TrackListAdapter(trackList);
        rvTrackList.setAdapter(trackListAdapter);
        rvTrackList.setLayoutManager(new GridLayoutManager(this, SPAN_COUNT));

        currentSearchOffset += trackList.size();
    }

    public void addTrackList(List<Track> trackList) {
        trackListAdapter.addTracks(trackList);
        trackListAdapter.notifyDataSetChanged();
        currentSearchOffset += trackList.size();
    }

    public void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
