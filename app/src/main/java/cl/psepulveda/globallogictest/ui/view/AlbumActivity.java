package cl.psepulveda.globallogictest.ui.view;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import cl.psepulveda.domain.Track;
import cl.psepulveda.globallogictest.R;
import cl.psepulveda.globallogictest.ui.adapter.AlbumTrackListAdapter;
import cl.psepulveda.globallogictest.ui.presenter.AlbumPresenter;

public class AlbumActivity extends BaseActivity {

    public static final String ALBUM_ID_EXTRA = "albumIdExtra";

    private AlbumPresenter presenter;
    private AlbumTrackListAdapter albumTrackListAdapter;

    private ImageView ivAlbumCover;
    private TextView tvAlbumName;
    private TextView tvArtistName;
    private RecyclerView rvAlbumTrackList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        ivAlbumCover = findViewById(R.id.ivAlbumCover);
        tvAlbumName = findViewById(R.id.tvAlbumName);
        tvArtistName = findViewById(R.id.tvArtistName);
        rvAlbumTrackList = findViewById(R.id.rvAlbumTrackList);

        rvAlbumTrackList.setLayoutManager(new LinearLayoutManager(this));
        rvAlbumTrackList.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        presenter = new AlbumPresenter(this);
        presenter.create(savedInstanceState);
    }

    public void renderAlbumData(List<Track> trackList) {

        Track albumInfo = trackList.get(0);
        trackList.remove(albumInfo);

        Picasso.with(this)
                .load(albumInfo.getArtworkUrl100())
                .into(ivAlbumCover);

        tvAlbumName.setText(albumInfo.getCollectionName());
        tvArtistName.setText(albumInfo.getArtistName());

        albumTrackListAdapter = new AlbumTrackListAdapter(trackList);

        rvAlbumTrackList.setAdapter(albumTrackListAdapter);

    }

    @Override
    public void onPause() {
        super.onPause();

        if(albumTrackListAdapter != null) {
            albumTrackListAdapter.pausePreview();
        }
    }

}
