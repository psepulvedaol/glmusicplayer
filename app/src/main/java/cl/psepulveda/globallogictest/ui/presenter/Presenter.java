package cl.psepulveda.globallogictest.ui.presenter;

import android.os.Bundle;

public interface Presenter {
    void create(Bundle savedInstanceState);

    void resume();

    void pause();

    void destroy();
}