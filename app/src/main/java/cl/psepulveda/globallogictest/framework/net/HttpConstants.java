package cl.psepulveda.globallogictest.framework.net;

public class HttpConstants {

    public static final String BASE_URL = "https://itunes.apple.com";
    public static final String SEARCH = "/search";
    public static final String ALBUM = "/lookup";

}
