package cl.psepulveda.usecases;

import cl.psepulveda.data.TrackRepository;
import cl.psepulveda.domain.callback.AlbumCallback;

public class GetAlbumById {

    private TrackRepository trackRepository;

    public GetAlbumById(TrackRepository trackRepository) {
        this.trackRepository = trackRepository;
    }

    public void getAlbumById(Long albumId, AlbumCallback callback) {
        trackRepository.getAlbumById(albumId, callback);
    }
}
