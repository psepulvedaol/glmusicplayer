package cl.psepulveda.globallogictest.framework.net.dto;

import java.util.List;

import cl.psepulveda.domain.Track;

public class SearchTrackResponse {

    private Integer resultCount;
    private List<Track> results;

    public SearchTrackResponse() {
    }

    public Integer getResultCount() {
        return resultCount;
    }

    public void setResultCount(Integer resultCount) {
        this.resultCount = resultCount;
    }

    public List<Track> getResults() {
        return results;
    }

    public void setResults(List<Track> results) {
        this.results = results;
    }
}
