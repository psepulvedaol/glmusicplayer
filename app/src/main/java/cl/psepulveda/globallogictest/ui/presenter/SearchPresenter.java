package cl.psepulveda.globallogictest.ui.presenter;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

import cl.psepulveda.data.TrackRepository;
import cl.psepulveda.domain.Track;
import cl.psepulveda.domain.callback.TrackListCallback;
import cl.psepulveda.globallogictest.R;
import cl.psepulveda.globallogictest.framework.RetrofitTrackDataSource;
import cl.psepulveda.globallogictest.ui.view.SearchActivity;
import cl.psepulveda.usecases.GetTrackListBySearch;

public class SearchPresenter implements Presenter {

    private SearchActivity view;
    private GetTrackListBySearch getTrackListBySearch;

    public SearchPresenter(SearchActivity view) {
        this.view = view;
        TrackRepository repository = new TrackRepository(new RetrofitTrackDataSource(view));
        getTrackListBySearch = new GetTrackListBySearch(repository);
    }

    @Override
    public void create(Bundle savedInstanceState) {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    public void searchButtonClicked(String text, int offset) {
        view.hideSoftKeyboard();

        getTrackListBySearch.getTrackListBySearch(text, offset, new TrackListCallback() {
            @Override
            public void onTrackListLoaded(List<Track> trackList) {
                view.renderTrackList(trackList);
            }

            @Override
            public void onTrackListError(Exception errorBundle) {
                view.showToast(view.getString(R.string.internet_connection_error));
            }
        });
    }

    public void loadMore(String text, int offset) {
        Log.d("LOAD_MORE", "Text: " + text);
        Log.d("LOAD_MORE", "Offset: " + offset);
        getTrackListBySearch.getTrackListBySearch(text, offset, new TrackListCallback() {
            @Override
            public void onTrackListLoaded(List<Track> trackList) {
                view.addTrackList(trackList);
            }

            @Override
            public void onTrackListError(Exception errorBundle) {
                view.showToast(view.getString(R.string.internet_connection_error));
            }
        });
    }
}
