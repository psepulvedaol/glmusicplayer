package cl.psepulveda.domain.callback;

import java.util.List;

import cl.psepulveda.domain.Track;

public interface TrackListCallback {
    void onTrackListLoaded(List<Track> trackList);
    void onTrackListError(Exception exception);
}
