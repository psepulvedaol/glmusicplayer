package cl.psepulveda.data;

import cl.psepulveda.domain.callback.AlbumCallback;
import cl.psepulveda.domain.callback.TrackListCallback;

public interface TrackDataSource {
    void getTrackBySearch(String searchText, int offset, TrackListCallback callback);

    void getAlbumById(Long albumId, AlbumCallback callback);
}
