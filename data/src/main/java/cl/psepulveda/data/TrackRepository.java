package cl.psepulveda.data;

import cl.psepulveda.domain.callback.AlbumCallback;
import cl.psepulveda.domain.callback.TrackListCallback;

public class TrackRepository {

    private TrackDataSource trackDataSource;

    public TrackRepository(TrackDataSource trackDataSource) {
        this.trackDataSource = trackDataSource;
    }

    public void getTrackListBySearch(String searchText, int offset, TrackListCallback callback) {
        trackDataSource.getTrackBySearch(searchText, offset, callback);
    }

    public void getAlbumById(Long albumId, AlbumCallback callback) {
        trackDataSource.getAlbumById(albumId, callback);
    }
}
