package cl.psepulveda.globallogictest.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cl.psepulveda.domain.Track;
import cl.psepulveda.globallogictest.R;
import cl.psepulveda.globallogictest.framework.audio.Player;

public class AlbumTrackListAdapter extends RecyclerView.Adapter<AlbumTrackListAdapter.TrackInfoHolder>{

    private List<Track> trackList;
    private Player audioPlayer;


    public AlbumTrackListAdapter(List<Track> trackList) {
        this.trackList = trackList;
        audioPlayer = new Player();
    }

    @NonNull
    @Override
    public TrackInfoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_album_track, viewGroup, false);

        TrackInfoHolder vh = new TrackInfoHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final TrackInfoHolder trackInfoHolder, int i) {
        final Track track = trackList.get(i);

        trackInfoHolder.tvTrack.setText(trackInfoHolder.itemView.getContext().getString(R.string.album_track, track.getTrackNumber(), track.getTrackName()));
        trackInfoHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(track.getPreviewUrl().equals(audioPlayer.getAudioUrl())) {
                    if(audioPlayer.isPlaying()) {
                        audioPlayer.pause();
                    } else {
                        audioPlayer.play();
                    }

                    return;
                }

                if(audioPlayer.isPlaying()) {
                    audioPlayer.pause();
                    audioPlayer.release();
                    audioPlayer = new Player();
                }
                audioPlayer.execute(track.getPreviewUrl());
            }
        });
    }

    @Override
    public int getItemCount() {
        return trackList.size();
    }

    public void pausePreview() {
        if(audioPlayer.isPlaying()) {
            audioPlayer.pause();
        }
    }

    static class TrackInfoHolder extends RecyclerView.ViewHolder {

        TextView tvTrack;
        //Player player;

        TrackInfoHolder(View v) {
            super(v);
            tvTrack = v.findViewById(R.id.tvTrack);
        }
    }
}
