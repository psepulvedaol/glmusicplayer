package cl.psepulveda.globallogictest.framework.audio;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;

public class Player extends AsyncTask<String, Void, Boolean> {

    private MediaPlayer mediaPlayer;
    private boolean prepared = false;
    private String url;

    public Player() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    @Override
    public Boolean doInBackground(String... strings) {

        try {
            url = strings[0];
            mediaPlayer.setDataSource(url);
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                }
            });

            mediaPlayer.prepare();
            prepared = true;

        } catch (Exception e) {
            Log.e("MyAudioStreamingApp", "" + e.getMessage());
            prepared = false;
        }

        return prepared;
    }

    @Override
    public void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        if(prepared) {
            mediaPlayer.start();
        }
    }

    public void pause() {
        if(mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
    }

    public void play() {
        if(prepared && !mediaPlayer.isPlaying()){
            mediaPlayer.start();
        }
    }

    public boolean isPlaying() {
        if(mediaPlayer != null) {
            return mediaPlayer.isPlaying();
        } else {
            return false;
        }
    }

    public String getAudioUrl() {
        return url;
    }

    public void release() {
        if(mediaPlayer != null) {
            mediaPlayer.release();
        }
    }
}
