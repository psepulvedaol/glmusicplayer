package cl.psepulveda.domain.callback;

import java.util.List;

import cl.psepulveda.domain.Track;

public interface AlbumCallback {
    void onAlbumLoaded(List<Track> trackList);
    void onError(Exception exception);
}
