package cl.psepulveda.globallogictest.ui.presenter;

import android.os.Bundle;

import java.util.List;

import cl.psepulveda.data.TrackRepository;
import cl.psepulveda.domain.Track;
import cl.psepulveda.domain.callback.AlbumCallback;
import cl.psepulveda.globallogictest.R;
import cl.psepulveda.globallogictest.framework.RetrofitTrackDataSource;
import cl.psepulveda.globallogictest.ui.view.AlbumActivity;
import cl.psepulveda.usecases.GetAlbumById;

public class AlbumPresenter implements Presenter {

    private AlbumActivity view;
    private GetAlbumById getAlbumById;

    public AlbumPresenter(AlbumActivity view) {
        this.view = view;
        TrackRepository repository = new TrackRepository(new RetrofitTrackDataSource(view));
        getAlbumById = new GetAlbumById(repository);
    }

    @Override
    public void create(Bundle savedInstanceState) {
        if(view.getIntent().hasExtra(AlbumActivity.ALBUM_ID_EXTRA)) {
            loadAlbumData(view.getIntent().getLongExtra(AlbumActivity.ALBUM_ID_EXTRA, 0));
        } else {
            view.finish();
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void loadAlbumData(Long albumId) {
        getAlbumById.getAlbumById(albumId, new AlbumCallback() {
            @Override
            public void onAlbumLoaded(List<Track> trackList) {
                view.renderAlbumData(trackList);
            }

            @Override
            public void onError(Exception exception) {
                view.showToast(view.getString(R.string.internet_connection_error));
            }
        });
    }
}
