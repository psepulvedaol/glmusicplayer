package cl.psepulveda.globallogictest.framework;

import android.content.Context;

import java.io.IOException;

import cl.psepulveda.data.TrackDataSource;
import cl.psepulveda.domain.callback.AlbumCallback;
import cl.psepulveda.domain.callback.TrackListCallback;
import cl.psepulveda.globallogictest.framework.net.HttpConstants;
import cl.psepulveda.globallogictest.framework.net.ITunesApiServices;
import cl.psepulveda.globallogictest.framework.net.dto.GetAlbumDetailResponse;
import cl.psepulveda.globallogictest.framework.net.dto.SearchTrackResponse;
import cl.psepulveda.globallogictest.framework.util.Utils;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitTrackDataSource implements TrackDataSource {

    private static final String MEDIA_TYPE = "music";
    private static final Integer TRACKS_LIMIT = 20;

    private static final String ENTITY = "song";

    private ITunesApiServices iTunesApiServices;

    public RetrofitTrackDataSource(Context context) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HttpConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(buildClient(context))
                .build();

        iTunesApiServices = retrofit.create(ITunesApiServices.class);
    }

    @Override
    public void getTrackBySearch(String searchText, int offset, final TrackListCallback callback) {

        iTunesApiServices.searchTrack(searchText, offset, MEDIA_TYPE, TRACKS_LIMIT).enqueue(new Callback<SearchTrackResponse>() {
            @Override
            public void onResponse(Call<SearchTrackResponse> call, Response<SearchTrackResponse> response) {
                if(response.code() == 200) {
                    callback.onTrackListLoaded(response.body().getResults());
                } else {
                    callback.onTrackListError(new Exception());
                }
            }

            @Override
            public void onFailure(Call<SearchTrackResponse> call, Throwable t) {
                callback.onTrackListError(new Exception(t));
            }
        });

    }

    @Override
    public void getAlbumById(Long albumId, final AlbumCallback callback) {
        iTunesApiServices.getAlbumById(albumId, ENTITY).enqueue(new Callback<GetAlbumDetailResponse>() {
            @Override
            public void onResponse(Call<GetAlbumDetailResponse> call, Response<GetAlbumDetailResponse> response) {
                if(response.code() == 200) {
                    callback.onAlbumLoaded(response.body().getResults());
                } else {
                    callback.onError(new Exception());
                }
            }

            @Override
            public void onFailure(Call<GetAlbumDetailResponse> call, Throwable t) {
                callback.onError(new Exception(t));
            }
        });
    }

    private OkHttpClient buildClient(final Context context) {
        Cache cache = new Cache(context.getCacheDir(), 5*1024*1024);
        return new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();

                        if(Utils.isOnline(context)) {
                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build();
                        } else {
                            request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build();
                        }

                        return chain.proceed(request);
                    }
                }).build();
    }
}
