package cl.psepulveda.usecases;

import cl.psepulveda.data.TrackRepository;
import cl.psepulveda.domain.callback.TrackListCallback;

public class GetTrackListBySearch {

    private TrackRepository trackRepository;

    public GetTrackListBySearch(TrackRepository trackRepository) {
        this.trackRepository = trackRepository;
    }

    public void getTrackListBySearch(String searchText, int offset, TrackListCallback callback) {
        trackRepository.getTrackListBySearch(searchText, offset, callback);
    }
}
