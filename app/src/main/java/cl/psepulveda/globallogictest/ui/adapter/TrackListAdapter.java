package cl.psepulveda.globallogictest.ui.adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import cl.psepulveda.domain.Track;
import cl.psepulveda.globallogictest.R;
import cl.psepulveda.globallogictest.ui.view.AlbumActivity;

public class TrackListAdapter extends RecyclerView.Adapter<TrackListAdapter.TrackInfoHolder>{

    private List<Track> trackList;

    public TrackListAdapter(List<Track> trackList) {
        this.trackList = trackList;
    }

    public void addTracks(List<Track> trackList) {
        this.trackList.addAll(trackList);
    }

    @NonNull
    @Override
    public TrackInfoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_track, viewGroup, false);

        TrackInfoHolder vh = new TrackInfoHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final TrackInfoHolder trackInfoHolder, int i) {
        final Track track = trackList.get(i);

        trackInfoHolder.trackName.setText(track.getTrackName());
        trackInfoHolder.tvArtistName.setText(track.getArtistName());
        Picasso.with(trackInfoHolder.itemView.getContext())
                .load(track.getArtworkUrl100())
                .centerCrop()
                .resize(100, 100)
                .into(trackInfoHolder.ivLogo);

        trackInfoHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(track.getTrackId() != null) {
                    Intent intent = new Intent(trackInfoHolder.itemView.getContext(), AlbumActivity.class);
                    intent.putExtra(AlbumActivity.ALBUM_ID_EXTRA, track.getCollectionId());
                    trackInfoHolder.itemView.getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return trackList.size();
    }

    static class TrackInfoHolder extends RecyclerView.ViewHolder {

        TextView trackName;
        TextView tvArtistName;
        ImageView ivLogo;

        public TrackInfoHolder(View v) {
            super(v);
            trackName = v.findViewById(R.id.tvTrackName);
            tvArtistName = v.findViewById(R.id.tvArtistName);
            ivLogo = v.findViewById(R.id.ivLogo);
        }
    }
}
